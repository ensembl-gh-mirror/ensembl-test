# Ensembl test harness and related utility scripts

[![Build Status](https://travis-ci.org/Ensembl/ensembl-test.svg?branch=release/102)][travis]

[travis]: https://travis-ci.org/Ensembl/ensembl-test
